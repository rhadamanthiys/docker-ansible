FROM ubuntu:18.04

# Labels.
LABEL maintainer="will@willhallonline.co.uk" \
    org.label-schema.schema-version="1.0" \
    org.label-schema.build-date=$BUILD_DATE \
    org.label-schema.vcs-ref=$VCS_REF \
    org.label-schema.name="willhallonline/ansible" \
    org.label-schema.description="Ansible inside Docker" \
    org.label-schema.url="https://github.com/willhallonline/docker-ansible" \
    org.label-schema.vcs-url="https://github.com/willhallonline/docker-ansible" \
    org.label-schema.vendor="Will Hall Online" \
    org.label-schema.docker.cmd="docker run --rm -it -v $(pwd):/ansible -v ~/.ssh/id_rsa:/root/id_rsa willhallonline/ansible:2.7-ubuntu-18.04"

RUN DEBIAN_FRONTEND=noninteractive apt-get update && \
    apt-get install -y gnupg2 python3-pip sshpass git openssh-client wget unzip jq curl  && \
    rm -rf /var/lib/apt/lists/* && \
    apt-get clean

RUN python3 -m pip install --upgrade pip cffi && \
    pip install ansible==2.9.14 && \
    pip install mitogen ansible-lint boto boto3 botocore && \
    pip install --upgrade pywinrm

RUN mkdir /ansible && \
    mkdir -p /etc/ansible && \
    echo 'localhost' > /etc/ansible/hosts

RUN wget https://releases.hashicorp.com/terraform/0.12.13/terraform_0.12.13_linux_amd64.zip && \
    unzip terraform_0.12.13_linux_amd64.zip -d /usr/local/bin/ &&  \
    rm terraform_0.12.13_linux_amd64.zip

RUN wget https://releases.hashicorp.com/packer/1.5.6/packer_1.5.6_linux_amd64.zip &&  \
    unzip packer_1.5.6_linux_amd64.zip -d /usr/local/bin/ &&  \
    rm packer_1.5.6_linux_amd64.zip

COPY requirements.yml /

RUN ansible-galaxy install -r /requirements.yml

WORKDIR /ansible

CMD [ "ansible-playbook", "--version" ]